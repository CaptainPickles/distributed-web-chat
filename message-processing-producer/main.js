import { createClient } from 'redis';

const client = createClient( { url: 'redis://redis:6379' })

client.on('error', (err) => console.log('Redis Client Error', err));


client.on('connect', () => {
    console.log('Connected to Redis proccessing producer');
  });

await client.connect();

const subscriber = client.duplicate();

await subscriber.connect();

const WORK_QUEUE_1 = "WORK_QUEUE_1_HATE_CLASSIFIER" 
const WORK_QUEUE_2 = "WORK_QUEUE_2_SUICIDAL_CLASSIFIER"

await subscriber.subscribe("message-processing",async (message) => {
    try {
    const promises = []
    promises.push(client.lPush(WORK_QUEUE_1,message))
    promises.push(client.lPush(WORK_QUEUE_2,message))
    await Promise.all(promises)
    // send multiple work queue

}
    catch (error) {
        console.log("message-processing error " ,error)
    }
  });