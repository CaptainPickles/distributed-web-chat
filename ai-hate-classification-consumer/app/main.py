from transformers import pipeline
import redis
from threading import Thread
import json
# load model first
classifier = pipeline("text-classification",model="Hate-speech-CNERG/bert-base-uncased-hatexplain") # classify 3 label hate speesh offensive or normal
print("Ai-hate-classification-consumer finished loading model")
r = redis.Redis(
   host="redis",
    port=6379)


class ConsumerAi:
    def __init__(self):
      self.client =  r

    def start(self):
      while True:
        payload = r.blpop('WORK_QUEUE_1_HATE_CLASSIFIER',0)
        if( payload is not None ):
          messageStringified = payload[1]
          data = json.loads(messageStringified)
          print(data)
          prediction = classifier(data.get('message'))
          data['hatePrediction'] = prediction[0]
          print(data)
          r.publish("HATE_SPEECH_CLASSIFICATION",json.dumps(data)) # return result maybe need to add id and datetime to result


consumerAi = ConsumerAi()

thread_1 = Thread(target=consumerAi.start,daemon=True)

thread_1.start()

while True:
  pass
