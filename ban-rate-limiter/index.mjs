import { createClient } from 'redis';

// Connexion au service Redis
const client = createClient({
    url: "redis://redis"
});

// On utilisera un subscriber pour écouter l'arrivée d'un message dans la channel "chat-room"
const subscriber = client.duplicate();
// Gestion des erreurs préconisé par le package node redis
client.on('error', (err) => console.log('Redis Client Error', err));
subscriber.on('error', (err) => console.log('Redis Subscribe Error', err));

await client.connect();
await subscriber.connect();

// Récupère les messages de la channel 'chat-room' et les affiche dans le terminal
await subscriber.subscribe('chat-room', async (message) => {
        // Convertir le message publié dans la channel en JSON et récupérer l'adresse IP
        const data = JSON.parse(message);
        const ip = data.ip;

        // On vérifie si l'adresse ip a déjà envoyé un message en moins de 10 secondes
        let checkIpExist = await client.exists(ip);

        // Nous créeons la clé de banissement au cas pour vérifier ensuite si l'adresse IP de l'utilisateur a été bannie ou non.
        const banKey = `ban:${ip}`;
        if(await client.get(banKey)){
            console.log(`${ip} is not allowed to publish a message.`);
            return;
        }

        if(checkIpExist == 0){
            // Si l'adresse ip n'a pas envoyé de message nous créons une clé avec l'adresse ip de l'utilisateur avec un expire de 10 secondes
            // Et nous initialisons son compteur de message à 1
            await client.set(ip, 1, {
                EX: 10,
                NX: true
            });
        }else{
            // Si l'utilisateur a déjà envoyé un message en moins de 10 secondes, on incrémente le compteur de son message pour son adresse IP
            client.incr(ip);
        }
        console.log(`${ip} send a message in the chat, he send ${await client.get(ip)} messages in less 10 seconds.`);

        // Si l'utilisateur a envoyé plus de 10 messages en moins de 10 secondes, alors nous le banissons pendant une période 2 minutes
        if(await client.exists(ip) && await client.get(ip) > 10){
            await client.set(banKey, 1, {
                EX: 120,
                NX: true
            });
            console.log(`${ip} send too many messages in less 10 seconds, ${ip} is now in banned list.`);
            return;
        }

        // Lorsque tout est ok, on envoit dans le channel qui va analyser le contenu du message (suicidaire ou violent)
        await client.publish('message-processing', message);
});
