import { createClient } from 'redis';
import { resourceLimits } from 'worker_threads';

const client = createClient( { url: 'redis://redis:6379' })

client.on('error', (err:any) => console.log('Redis Client Error', err));


client.on('connect', () => {
    console.log('Connected to Redis message aggregator');
  });

await client.connect();

const subscriber = client.duplicate();

await subscriber.connect();

const channelToSubscribe = ["HATE_SPEECH_CLASSIFICATION","SUICIDAL-SPEECH-CLASSIFICATION"]
const lastMessageSend = "LAST_MESSAGES_SEND"

interface Message {
  ip:string
  date :Date,
  message:string
}
interface AiHateClassificationMessage extends Message {
  hatePrediction : {label:"normal" | "offensive" | "hate speech", score :number}
}

interface AiSuicidalClassifcationMessahe extends Message {
  isSuicidalMessage : boolean
}
function instanceOfMessage(object: any): object is Message {
  return typeof object.ip === "string" && typeof object.date === "string";
}
function isAiHateClassificationMessage(object: any): object is AiHateClassificationMessage {
  return typeof object.hatePrediction === "object" && typeof object.hatePrediction.label === "string" && typeof object.hatePrediction.score === "number" ;
}
function isAiSuicidalClassificationMessage(object: any): object is AiSuicidalClassifcationMessahe {
  return typeof object.isSuicidalMessage === "boolean"
}

const MAX_MESSAGES_IN_LIST = 50
let hateList : AiHateClassificationMessage[]  = []
let suicidalList : AiSuicidalClassifcationMessahe[]  = []

await subscriber.subscribe(channelToSubscribe,async (data:string,channel:string) => {
    try {
      console.log(`receiver received from channel ${channel}`,data)
     
      const parsedMessage =  JSON.parse(data)
    if(channel === channelToSubscribe[0]){
      console.log("instanceOfMessage ", instanceOfMessage(parsedMessage) , " isAiHateClassificationMessage ", isAiHateClassificationMessage(parsedMessage))
    if(instanceOfMessage(parsedMessage) && isAiHateClassificationMessage(parsedMessage) )    {
     await handleHateMessage(parsedMessage)
    }  

    }
    if (channel === channelToSubscribe[1]){
      console.log("instanceOfMessage ", instanceOfMessage(parsedMessage) , " isAiSuicidalClassificationMessage ", isAiSuicidalClassificationMessage(parsedMessage))
      if(instanceOfMessage(parsedMessage) && isAiSuicidalClassificationMessage(parsedMessage)){
        await handleSuicidalMessage(parsedMessage)
      }
    }

}
    catch (error) {
        console.log("message aggregator " ,error)
    }
  });

async function handleHateMessage(message : AiHateClassificationMessage){
  console.log("handleHateMessage ", hateList )
  const founndMessageIndex = suicidalList.findIndex(m => m.date === message.date && m.ip === message.ip)
  if(founndMessageIndex !== -1){
    const result = Object.assign(message,suicidalList[founndMessageIndex])
    suicidalList.splice(founndMessageIndex,1) // remove for memory issue 
    if(message.hatePrediction.label === "hate speech"){
      await client.set(`ban:${message.ip}`,1,{EX:3600,NX:true})
      console.log(`${message.ip} has been banned for saying : ${message.message}`)
    }else {
    await publishAggregateMessage(result)
    }
  }else {
    hateList.push(message)
  }

}

async function handleSuicidalMessage(message : AiSuicidalClassifcationMessahe){
  console.log("handleSuicidalMessage ", suicidalList )

  const founndMessageIndex = hateList.findIndex(m => m.date === message.date && m.ip === message.ip)
  if(founndMessageIndex !== -1){
    const result = Object.assign(message,hateList[founndMessageIndex])
    hateList.splice(founndMessageIndex,1) // remove for memory issue 
    await publishAggregateMessage(result)
  }else {
    suicidalList.push(message)
  }

}

async function publishAggregateMessage(message:Message){
  console.log("publishAggregateMessage ", message.message)
  await client.publish(lastMessageSend, JSON.stringify(message));
}