const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const redis = require('redis');
const util = require('util');
var last_message_filter = "";

// create a new Redis client
const redisClient = redis.createClient({
  url: "redis://redis"
});
const subscriber = redisClient.duplicate();

redisClient.connect().then(() => {
  console.log("Redis connected");
})

subscriber.connect().then(() => {
  console.log("Redis subscriber connected");
})

async function Subs() {
  await subscriber.subscribe('LAST_MESSAGES_SEND', async (message) => {

    console.log('coucou uwu')
    const messageObj = JSON.parse(message)
    console.log(messageObj);
    last_message_filter = messageObj;
    if(!messageObj.currentRoom) return
    // await redisClient.rPush(messageObj.currentRoom + '-msg', JSON.stringify({ id: messageObj.pseudo, message: messageObj.message }));
   io.in(messageObj.currentRoom).emit('new message', messageObj);
  })
}

Subs()

  

// use the Redis client to get the total number of users in a room

const getRoomUsers = room => {
  const hGetAllAsync = util.promisify(redisClient.hGetAll).bind(redisClient);

  return hGetAllAsync(room)
    .then(users => {
      return users ? Object.values(users) : 0;
    })
    .catch(error => {
      throw error;
    });
};

/*
const getRoomMessages = room => new Promise(async (resolve, reject) => {
  await redisClient.lRange(room + '-msg', 0, -1, (err, messages) => {
    if (err) {
      reject(err);
    } else {
      
      resolve(messages.map(message => JSON.parse(message)));
    }
  });
});*/

const getOldMessages = async (currentRoom) => {
  console.log("étape old message 1")
  return new Promise((resolve, reject) => {
    console.log("étape old message 2")
    redisClient.hVals(currentRoom, (error, messages) => {
      console.log("étape old message 3")
      if (error) {

        reject(error);
      } else {
       console.log("étape old message reussi")
        resolve(messages);
      }
    });
  });
};
io.on('connection', socket => {
  console.log('New connexion UwU')
  let currentRoom;

  // when the client emits 'joinRoom', this listens and executes
  socket.on('joinRoom', async room => {
    console.log("current room :",room)
    console.log("old room", currentRoom)
    // leave the current room (if any)
    if (currentRoom) {
      console.log("joinroom 1")
      socket.leave(currentRoom);
      console.log("joinroom 2")
      socket.emit('disconnected')
      console.log("joinroom 3")
      //const users = await getRoomUsers(currentRoom);
      console.log("joinroom 4")
      // update the total number of users in the current room
      //io.in(currentRoom).emit('updateUsers', users);
      console.log("joinroom 5")
    }

    // join the new room
    currentRoom = room;
    socket.join(currentRoom);


    redisClient.hSet(currentRoom, socket.pseudo, '127.0.0.1');
    console.log("room rejoin:", currentRoom)
    //const messages = await getOldMessages(currentRoom);
    //socket.emit('old message', messages);

    // update the total number of users in the new room
    //const users = await getRoomUsers(currentRoom);

    //io.in(currentRoom).emit('updateUsers', users);
    /*
    try {
      const messages = await redisClient.lRange(currentRoom, 0, -1);
      console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",messages)
      socket.emit('old message', messages);
    } catch (error) {
      console.error(error);
    console.log("message test 0")
    }
    */

  });
  // when the client emits 'new message', this listens and executes
  socket.on('new message', async message => {
    const messageObject = { date: new Date(), message: message ,ip:socket.handshake.address, currentRoom: currentRoom, pseudo: socket.pseudo }
    await redisClient.publish('chat-room', JSON.stringify(messageObject));
    // // store the message in Redis
    // redisClient.rPush(currentRoom + '-msg', JSON.stringify({ id: socket.pseudo, message }));

    // broadcast the message to all users in the current room
    // io.in(currentRoom).emit('new message', socket.pseudo, message);
  });

  // when the client emits 'typing', we broadcast it to others in the current room
  socket.on('typing', () => {
    socket.to(currentRoom).emit('typing', socket.pseudo);
  });

  // when the client emits 'stop typing', we broadcast it to others in the current room
  socket.on('stop typing', () => {
    socket.to(currentRoom).emit('stop typing', socket.pseudo);
  });

  socket.on('login', (pseudo) => {
    socket.pseudo = pseudo;
  });

  // when the user disconnects, remove their data from Redis and update the total number of users in the current room
  socket.on('disconnect', async () => {
    if (currentRoom) {
      redisClient.hDel(currentRoom, socket.pseudo);
      //const users = await getRoomUsers(currentRoom);
      //io.in(currentRoom).emit('updateUsers', users);
    }
  });
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

server.listen(3000, () => {
  console.log('Listening on *:3000');
});