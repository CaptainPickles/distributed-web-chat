
⚠ pour le service d'auth en local: il vous faut node 18+

(avec nvm: `nvm install --lts`)

1. `make npm-install-spa`
3. `make build-frontend`
4. `docker compose up`

# Both service load ai model
And it takes a while before you can send messages since you need to wait for model to be fully loaded,
to check if model are loaded logs into hate and suicidal cassification services
`docker logs -f nameOfService`
And check if all download are completed

# To launch compose for dev
`docker compose -f docker-compose-dev.yml up --build`


redis

# Redis insight client  

Access with localhost:8001

# Enter in REDIS CLI
`docker exec -it redis redis-cli` once docker compose is up

# To publish with redis CLI 
`PUBLISH channel message`

