import { createRouter, createWebHistory } from 'vue-router'
import { ref } from 'vue';
import Home from '../views/Home.vue'
import Error from '../views/Error.vue'

const pseudoRef = ref(JSON.parse(localStorage.getItem('pseudo')))
const socket = ref("")

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			component: Home,
			name: 'home'
		},
		{
			path: '/channel/:id',
			component: () => import('../views/Channel.vue'),
			name : 'channel'
		},
		{
			path: '/:pathMatch(.*)*',
			component: Error,
			name: 'error'
		  }
	],
})

router.beforeEach((to, from, next) => {
	if (to.path !== '/' && !to.path.startsWith('/channel/')) {
		// La route n'existe pas ou ne commence pas par "/channel/"
		next({ name: 'error' }); // Redirige vers la route nommée "error"
	  }
	else if (to.path.startsWith('/channel/') && pseudoRef.value == null) {
	  // La route commence par "/channel/" et pseudoRef est null
	  next('/'); // Redirige vers la route "/home"
	} else {
	  next(); // Permet de continuer vers la route demandée
	}

  });


export default router