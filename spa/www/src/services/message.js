import { useRouter } from 'vue-router';

const router = useRouter(); 
var socket = null;

const messages = []
const message = ''

let state = 0 ;

function sendMessage(){

    socket.emit('message', this.message);
    this.message = '';
}

function setUsername(){
    socket.emit('join', this.username);
          this.username = '';
          this.state = 1;
          this.router.push({name: "channel-1"})
}

function created() {
    socket = io();
}
function mounted() {
    socket.on('message', function (message) {
        app.messages.push(message);
        // this needs to be done AFTER vue updates the page!!
        app.$nextTick(function () {
          var messageBox = document.getElementById('chatbox');
          messageBox.scrollTop = messageBox.scrollHeight;
        });
      });
}

function useMessage(){
    return{sendMessage,setUsername,created,mounted}
}

export{useMessage}