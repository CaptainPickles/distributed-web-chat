import io from 'socket.io-client';



const socket = io('http://socketio.localhost',{
  // WARNING: in that case, there is no fallback to long-polling
  transports: ["websocket"] // or [ "websocket", "polling" ] (the order matters)
});


/*
  function sendMessage(message){
    socket.emit('message', {message: message});
    message = '';
}
*/
export { socket };